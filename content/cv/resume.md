+++
title = "Resumé"
description = "Hugo, the world's fastest framework for building websites"
date = "2019-02-28"
aliases = ["about-us", "about-hugo", "contact"]
author = "Hugo Authors"
+++

# Machine Learning Focus:
### University of Helsinki 
#### 2022
"Ethics of AI" -
Certificate [here](https://certificates.mooc.fi/validate/kpm4mjy4cp)

### Kaggle - Online
#### 2021
"Pandas" - 
Certificate: [here](https://www.kaggle.com/learn/certification/tiboas/pandas)

"Machine Learning Explainability" - 
Certificate: [here](https://www.kaggle.com/learn/certification/tiboas/machine-learning-explainability)

### Deeplearning.ai - Coursera, Online 
#### 2019
**Deep learning Specialization:** - Certificate: [here](https://coursera.org/share/b667b2ea50892f1f3559ac3e2831d4f5)\
”Neural Networks and Deep Learning” <br>
”Convolutional Neural Networks” <br>
”Improving Deep Neural Networks: Hyperparameter tuning, Regularization and Optimization” <br>
”Sequence Models” <br>
"Structuring Machine Learning Projects” 

### Published several papers on neural networks in various applications in optical communication:
Please see: <u><a href="../../publications">Selected publications</a> </u><br>
*Main topics:*<br>
-- Using autoencoders to learn to communicate over nonlinear optical channels <br>
-- Risk of overfitting in optical experiments with pseudo random pattern generators <br>

### Academic recognition in machine learning:
2023: Evaluation committee member for PhD defence on "Autoencoders for Physical-Layer Communications: Approaches and Applications" <br>
Journal of Lightwave Technology - 2018 Best Paper award for "End-to-End Deep Learning of Optical Fiber Communications"<br>
Invited workshop speaker: "Machine Learning for Optical Network and Transmission - Why and Where?," 2019 OptoElectronics and Communications Conference (OECC), Fukuoka, Japan <br>


### Other Courses:
Image Analysis - Chalmers University of Technology <br>
Statistical Image Analysis - Chalmers University of Technology <br>

# Programing Languages and other skills
Keras with Tensorflow<br>
Python <br>
_Packages such as:_ numpy, matplotlib, pandas, scipy, etc. <br>
Matlab  <br>
Caffe <br>
Dabbling with C++ / Mex for Matlab <br>
LaTex <br>
Git

# Professional Experience

### Infinera - Stockholm, Sweden. 
#### 2020 - Ongoing
##### Principal Optical Engineer        
Lead optical engineer at Stockholm site for point-to-multipoint coherent optical pluggable modules.
- Responisble for R&D, test and verification and interfacing to other groups.
- Lab automation using python.
- Digital signal processing algorithms in Python and Matlab.

Several side projects on machine learning in optical communication.

### Nat. Inst. of Information and Communications Technology - Tokyo, Japan. 
#### 2018 - 2019
##### Senior Visiting Researcher
One year as direct hire and one year on a fellowship from Vetenskapsrådet together with KTH, Stockholm.
- Started up and lead the development of a quantum key distribution test-bed.
- Coordinated a multinational team of researchers.
- Achieved record breaking results in less than two years with publications in highly ranked journals and invitations to prestigious conferences.

Side projects on end-to-end deep learning of nonlinear optical channels.


### Nokia Bell Labs - Stuttgart, Germany. 
#### 2016 - 2018
##### Researcher
- Research and development on metro and long-haul optical communication systems.
- Digital signal processing algorithm development in Matlab and Python.
- Strong focus on patent application.
- Driving awareness in optical communication community on the risks of overfitting when machine learning is applied in different use cases.
- End-to-end deep learning of optical communication systems.


###### Research Visits 
### Nat. Inst. of Information and Communications Technology - Tokyo, Japan. 
#### 3 months, 2014
- Developing high dimensional modulation formats for multicore fiber networks
- Implemented transmitter and receiver algorithms and built experimental setup investigating such formats
- Developed method and built setup for investigating impact of core-to-core crosstalk in multicore fibers

### Fraunhofer Heinrich Hertz Institute (HHI) - Berlin, Germany. 
#### 2 weeks, 2014
- Lead a fast paced project on a lattice based 4-dimensional modulation format for optical communication including experimental verification
- Was one of the first experimental demonstration of geometrically shaped modulation formats





# Education
### Chalmers University of Technology - Göteborg, Sweden. 
#### 2011-2015
##### PhD 
Thesis: “Multidimensional Modulation Formats for Coherent Single-and Multi-Core Fiber-Optical Communication Systems"
- Experimental and theoretical work on modulation formats for coherent communication systems.
- Developed coherent testbed and digital signal processing environment 
- Pioneer work on mutual information estimates on the fiber channel 
- Initiated and drove collaboration between the communication theory department and the experimental work at the photonics lab

### Chalmers University of Technology - Göteborg, Sweden. 
#### 2009-2011
##### Master of Science - Wireless and Photonics Engineering 
Master Thesis: “Real-time Parallel Optical Sampling"

### Chalmers University of Technology - Göteborg, Sweden. 
#### 2009-2011
##### Bachelor of Science - Electrical Engineering 
Bachelor Thesis: “High-resolution LiDAR-images for individual tree measurements"


###### Leadership Development 
### University of Michigan - Coursera, Online 
#### 2019
**Leading People and Teams Specialization:**\
”LeadingTeams”, ”Influencing People”, ”Managing Talent”, and ”Inspiring and Motivating Individuals"

### University of Skövde
#### 2019
”Project Management"

### Chalmers University of Technology
#### 2013-2015
”Creating and Managing Effective Teams”, ”Teaching, Learning and Evaluation”


# Scholarships and Academic Distinctions
###### Ranked Top 2% Scientist by Stanford
2021, 2020, and 2021 <br>
[Link to 2021 ranking list](https://elsevier.digitalcommonsdata.com/datasets/btchxktzyw/5)

###### Invited Talks
2022: Advanced Photonics Congress - The Netherlands <br>
2020: European Conference on Optical Communication (ECOC) - Belgium <br> 
2019: Advanced Photonics Congress / Signal Processing in Photonic Communications - USA <br> 
2018: Latin America Optics and Photonics Conference - Peru <br> 
2016: European Conference on Optical Communication (ECOC) - Germany <br> 
2016: Photonics West - USA

###### Scholarships 
2018: Vetenskapsrådet (VR) International Postdoc Fellowship with KTH and NICT <br> 
2018: Accepted Japan Society for the Promotion of Science Postdoc Scholarship - Not enrolled due to VR Postdoc <br> 
2016: Accepted Marie Curie Individual Fellowship with HHI - Not enrolled due to position at Nokia Bell Labs <br> 
**Travel grants**: Wenner-Gren Foundations, Sweden-Japan Foundation, The ÅForsk Foundation

<!-- ###### Other 
2018: Journal of Lightwave Technology - Best paper award
-->

###### Reviewer and conference committee engagement 

**Subcommitte member for Optical Communication Conference (OFC) 2019-Ongoing**: One of the major conferences in optical communication


**Acted as reviewer for several journals:** Nature Communications, IEEE Journal of Lightwave Technology, IEEE Transactions on Communications, Optics Express, Photonics Technology Letters,and more

# Other

###### Internships and Summer Jobs 
### Saab Defence -  Göteborg, Sweden. 
#### Summer, 2010
Software engineer - MMI Design for LiDAR

### Saab Defence -  Göteborg, Sweden. 
#### Summer, 2009
Radar testing

### Sandvik AB -  Sandviken, Sweden. 
#### Summer, 2006, 2007 and 2008
Chemical Analyst

###### Miscellaneous
**Student Representative** - Photonics Lab Executive Group, Chalmers University <br>
**Tutorial and Lab Teaching** - Several Courses, Chalmers University <br>
**Supervision of Bachelor Projects** - On wireless communication using LEDs, Chalmers University <br>
**Supervision of Visiting PhD Students** - Various topics, Nokia Bell Labs and NICT <br>