+++
title = "home"
description = ""
date = "2019-02-28"
aliases = ["home"]
author = "T. Eriksson"
+++



* [Projects]({{< ref "/content/projects.md" >}} "Projects")
* [Resumé]({{< ref "/content/cv/resume.md" >}} "Resumé")
* [Publications]({{< ref "/content/publications.md" >}} "Selected Publications")
