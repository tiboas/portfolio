+++
author = "T. Eriksson"
title = "3D CNNs for Brain Tumor Classification"
date = "2021-11-07"
description = ""
tags = [
    "Keras",
    "3D-CNN",
    "MRI scans"
]
thumbnail= "3DCNN.png"
+++


Main points:
* Data analysis and preprocessing for stacking 3D images from largely varying 3D scan data 
* 3D Convolutional Neural Network implemented with Keras

**Notebook best viewed on Kaggle:** https://www.kaggle.com/tiboas/3d-cnns-for-brain-tumor-classification

<iframe width="100%" height="18000" name="notebook" src="../../3d-cnns-for-brain-tumor-classification.html" frameBorder="0" scrolling="no" style="border:none;></iframe>

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
