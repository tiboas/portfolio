+++
author = "T. Eriksson"
title = "Image Classification using CNNs"
date = "2021-02-21"
description = ""
tags = [
    "Keras",
    "CNN",
]
thumbnail= "header.png"
+++

Main points:
* Training a convolutional neural network from scratch using Keras. 
* Image classification of foliar diseases in apple trees with 4 different classes
* Implemented in Keras


**Notebook best viewed on Kaggle:** https://www.kaggle.com/tiboas/keras-cnn-for-plant-pathology-2020

<iframe width="100%" height="20000" name="notebook" src="../../keras-cnn-for-plant-pathology-2020.html" frameBorder="0" scrolling="no" style="border:none;></iframe>


{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}



