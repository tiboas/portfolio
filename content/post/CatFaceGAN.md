+++
author = "T. Eriksson"
title = "Cat Face Generator GAN"
date = "2022-02-01"
description = ""
tags = [
    "Keras",
    "GAN"
]
thumbnail= "catface.png"
+++


Main points:
* Implement a deep convolutional generative adversarial network (GAN) in Keras
* Using color images of size 64x64
* Implemented several tip and tricks to help the GAN converge such as label smoothing and a version of the top-k training method 

Takeaways:
* Really hard to quantify when to stop training - what looks convincing to a human eye?
* Learnt a lot regarding different tricks to get good results and convergence 

**Notebook best viewed on Kaggle:** https://www.kaggle.com/tiboas/catfacegenerator-gan

<iframe width="100%" height="24500" name="notebook" src="../../catfacegenerator-gan.html" frameBorder="0" scrolling="no" style="border:none;></iframe>

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
