+++
author = "T. Eriksson"
title = "End-to-end Learning "
date = "2021-02-21"
description = ""
tags = [
    "Keras",
    "Autoencoder",
    "Communication"
]
thumbnail= "endtoend.png"
+++

**Main points:**
* Using an autoencoder structure to learn how to communicate over a channel 
* This work shows learning for the memoryless AWGN channel
* Tutorial-like implementation of the method used in our publications:\
[1] Karanov, B., Chagnon, M., Thouin, F., Eriksson, T. A., Bülow, H., Lavery, D., ... & Schmalen, L. (2018). End-to-end deep learning of optical fiber communications. Journal of Lightwave Technology, 36(20), 4843-4855. \
[2] Jones, R. T., Eriksson, T. A., Yankov, M. P., & Zibar, D. (2018, September). Deep learning of geometric constellation shaping including fiber nonlinearities. In 2018 European Conference on Optical Communication (ECOC)


**Notebook best viewed on Colab:**  https://colab.research.google.com/github/tiboas/End-to-end-Learning/blob/main/End_to_end_Learning_AWGN_channel.ipynb \
**Also on Github (videos not working):** https://github.com/tiboas/End-to-end-Learning

<iframe width="100%" height="14000" name="notebook" src="../../End_to_end_Learning_AWGN_channel.html" frameBorder="0" scrolling="no" style="border:none;></iframe>


{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}



