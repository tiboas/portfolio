+++
author = "T. Erikssons"
title = "Neural Networked based Mutual Information Estimator"
date = "2021-02-21"
description = ""
tags = [
    "Neural Networks",
    "Mutual Information",
]
thumbnail= "mi.png"
+++

Main points:
* Using neural networks to estimate a lower bound on the mutual information of unknown communication channels. 
* Shows example for 16QAM modulation over a AWGN channel an a phase-noise channel

Notebook best viewed on Github: https://github.com/tiboas/NeuralMI


<iframe width="100%" height="31000" name="notebook" src="../../NeuralNetwork_MIestimator.html" frameBorder="0" scrolling="no" style="border:none;></iframe>

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
