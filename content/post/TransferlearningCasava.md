+++
author = "Hugo Authors"
title = "Transfer learning for Plant Disease Identification from Photos"
date = "2021-06-21"
description = ""
tags = [
    "Keras",
    "CNN",
    "Transfer Learning"
]
thumbnail= "cassava.png"
+++


Main points:
* Implement transfer learning EfficientNetB4 
* Stratified KFold cross-validation
* Train and test-time image augmentation

**Notebook best viewed on Kaggle:** https://www.kaggle.com/tiboas/summary-cassava-trans-learn-stratified-kfold

<iframe width="100%" height="17000" name="notebook" src="../../summary-cassava-trans-learn-stratified-kfold.html" frameBorder="0" scrolling="no" style="border:none;></iframe>

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
