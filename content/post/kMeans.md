+++
author = "T. Eriksson"
title = "K-means clustering for QAM signals"
date = "2021-01-13"
description = ""
tags = [
    "k-means clustering"
]
thumbnail= "kmeans.png"
+++

Main points:
* Demonstrating k-means clustering for a 16-QAM signal under different distortions
* Discusses issues with convergence and showcase one way this can be solved for QAM signal 

Notebook best viewed on Github: https://github.com/tiboas/KmeansForQAM


<iframe width="100%" height="18500" name="notebook" src="../../K_means_clustering_for_QAM.html" frameBorder="0" scrolling="no" style="border:none;></iframe>

{{< css.inline >}}
<style>
.canon { background: white; width: 100%; height: auto; }
</style>
{{< /css.inline >}}
