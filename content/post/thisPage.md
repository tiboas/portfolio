+++
author = "T. Eriksson"
title = "This Webpage"
date = "2020-05-01"
description = ""
tags = [
    "This Webpage",
    "Hugo",
]
thumbnail= "thisPage.png"
+++

Main points:
* Portofolio page using Hugo framework
* Displayed using Gitlab pages


Repo available at: https://gitlab.com/tiboas/portfolio/


