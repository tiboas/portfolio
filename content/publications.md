+++
title = ""
description = ""
date = ""
aliases = []
author = "Tobias Eriksson"
+++

# Citations 
*Updated - 2024-02-06*<br>
![citations](../citations.PNG)

For a full list of all **100+** publications, see: https://scholar.google.com/citations?user=pEw1f9YAAAAJ&hl=sv

## Machine learning publications:
[1] TA Eriksson, D Villafani, et al., "*Neural Network Based Black-box Estimator of Mutual Information,*", Signal Processing in Photonic Communications (SPPCom), 2022.

[2] TA Eriksson, H Bülow, and A Leven,  "*Applying neural networks in optical communication systems: possible pitfalls,*" Photonics Technology Letter 29(23), 2091-2094, 2017 <br>

[3] B Karanov, M Chagnon, F Thouin, TA Eriksson, et al.,  "*End-to-end deep learning of optical fiber communications,*" Journal of Lightwave Technology 36(20), 4843-4855, 2018 *<br>

[4] RT Jones, TA Eriksson, et al.,  "*Deep learning of geometric constellation shaping including fiber nonlinearities,*" European Conference on Optical Communication (ECOC), 2018 <br>

[5] RT Jones, TA Eriksson, et al., "*Geometric constellation shaping for fiber optic communication systems via end-to-end learning,*" arXiv preprint arXiv:1810.00774, 2018 <br>
  
\* Journal of Lightwave Technology Best Paper Award 2018

 ## Other selected publications: 
 [1] TA Eriksson, et al., "*Wavelength division multiplexing of continuous variable quantum key distribution and 18.3 Tbit/s data channels,*" Nature Communications Physics 2 (1), 1-8, 2019 <br>

 [2] TA Eriksson, et al., "*Impact of 4D Channel Distribution on the Achievable Rates in Coherent Optical Communication Experiments,*" Journal of Lightwave Technology 34 (9), 2256-2266, 2016 <br>

 [3] TA Eriksson, et al., "*Four-dimensional estimates of mutual information in coherent optical communication experiments,*" European Conference on Optical Communication (ECOC), 2015 <br>

 [4] TA Eriksson, et al., "*Experimental investigation of a four-dimensional 256-ary lattice-based modulation format,*" Optical Fiber Communication Conference (OFC), 2015 <br>